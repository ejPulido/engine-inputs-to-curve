const objetoHtmlDefault = {
   id: "",
   class: " d-flex align-items-center justify-content-center w-100 h-100 mb-2",
   styles: "background-color: green;  position: relative;",
   chields: {
      div_parent: {
         //class: "popup divParent justify-content-center",
         chields: {
            div: {
               class: "d-flex justify-content-end orange-bg",
               chields: {
                  btnClose: {
                     type: "button",
                     id: "closeKeyBoard",
                     class: "closed buttonAux mr-2",
                  },
               },
            },
            div_1: {
               content: "Default Content",
               type: "p",
               id: "error",
               class: "rounded d-flex justify-content-center",
               styles: "background-color: grey; width: 280px; min-height: 150px;",
            },
         },
      },
   },
};

function gTime(def, mode = true, reset = true) {
   if (def.statusValidate === undefined) {
      def.statusValidate = false;
   };
   if (!def.statusValidate) {
      if (mode) {
         if (!def.timerState) {
            def.timer = setInterval(function () {
               def.timeInteraction++;
               if (def.debug) {
                  console.log(def.timeInteraction);
               };
            }, 1000);
            def.timerState = true;
         };
      } else {
         if (def.timer) {
            clearInterval(def.timer);
            def.timerState = false;
            if (reset) {
               def.timeInteraction = 0;
               def.statusValidate = true;
            };
         };
      };
   };
};

function gAxies({
   def,
   board,
   axie,
   position,
   values,
   color = '#000000',
} = {}) {
   if (!axie) {
      console.log('ponle una direction valueAxis:{ xd/yd: [[0, 0], [1, 0]],}');
      return;
   };

   const auxAxie = board.create('axis', axie);
   board.create('ticks', [auxAxie, position || false], {
      drawLabels: true,
      labels: values || false,
      label: {
         autoPosition: true,
         offset: [-5, 0],
         anchorX: 'middle',
         anchorY: 'top',
         visible: true,
      },
   });

   auxAxie.setAttribute({
      ticks: { visible: false },
   });
};

//points: [{ x: -3, y: 0, visible: true, interactive: true, fixed: true },]
function gAllPointsDefault(params = {}) {
   return params.points.map((p, i) => {
      let point = null;
      if (Array.isArray(p)) {
         point = params.board.create('point', [p[0], p[1]], {
            size: p[4] || 2,
            name: p[3] != undefined ? p[3] : '',
            label: {
               visible: undefined != p[3] && p[3] != '' ? true : false,
               autoPosition: true,
               offset: [0, 10],
            },
            fixed: false,
            visible: p[2] == undefined ? false : p[2],
            fillcolor: typeof p[5] == 'string' ? p[5] : p[5] ? 'white' : '#D55E00',
            strokeColor: typeof p[5] == 'string' ? p[5] : '#D55E00',
            ...p
         });
      } else {
         point = params.board.create('point', [p.x, p.y], {
            size: 2,
            label: {
               visible: p.name != undefined,
               autoPosition: true,
               offset: [0, 10],
            },
            fixed: false,
            visible: false,
            fillcolor: '#D55E00',
            strokeColor: '#D55E00',
            ...p,
         });
      }
      point.artifactNumber = params.i;
      point.ignore = params.ignore;
      point.iMod = i;

      if (p.interactive) {
         point.on('down', () => params.callback({ ...params, point, i }));
      };
      return point;
   });
};

function gLineDefault(params = {}) {
   if (!Array.isArray(params.lines)) {
      params.lines = [params.lines];
   };
   return params.lines.map((l) => {
      const line = params.board.create(
         'line',
         typeof l.points[0] == 'object' && typeof l.points[1] == 'object'
            ? l.points
            : gAllPointsDefault({ ...params, points: l.points }),
         {
            strokeColor: 'black',
            fixed: true,
            straightFirst: false,
            straightLast: false,
            firstArrow: false,
            lastArrow: false,
            strokeWidth: 2,
            name: l.name,
            label: {
               visible: l.name,
               autoPosition: true,

            },
            precision: {
               touch: 8,
               mouse: 3,
               pen: 5,
               hasPoint: 1,
            },
            ...l
         },
      );
      line.iMod = params.iMod;
      line.typeCurve = l.typeCurve;
      if (l.name) {
         gTextDefault({ ...params, texts: [l.name] });
      }
      if (l.interactive) {
         line.on('down', () => params.callback({ ...params, attractor: line }));
      };
      return line;
   });
};

/* curves: [{
   name: { text: "j", x: 2, y: 1, }, interactive: true, form: 0,
   points: [[-3, -3], [-2, -1], [0, 0], [2, 1], [3, 2.95]],
}] */
function gCurveDefault(params = {}) {
   return params.curves.map((c) => {
      const style = {
         strokeColor: 'black',
         strokeWidth: 1.5,
         fixed: true,
         label: {
            autoPosition: true,
            visible: true
         },
         precision: {
            touch: 8,
            mouse: 3,
            pen: 5,
            hasPoint: 1,
         },
         ...c,
      };
      const cAux = params.board.create(
         'cardinalspline',
         [gAllPointsDefault({ ...params, points: c.points }), c.form != undefined ? c.form : 1, 'centripetal'], style);
      if (c.name) {
         gTextDefault({ ...params, texts: [c.name] });
      }
      cAux.iMod = params.iMod;
      cAux.typeCurve = c.typeCurve || "curve";
      if (c.interactive) {
         cAux.on('down', () => params.callback({ ...params, attractor: cAux }));
      };
      return cAux;
   });
};

function gPolygon(params = {}) {
   return params.polygons.map((polygon) => {
      if (polygon.name) {
         gTextDefault({ ...params, texts: [polygon.name] });
      };
      return params.board.create('polygon', polygon.points, {
         fixed: true,
         withLines: false,
         fillColor: 'grey',
         fillOpacity: 0.1,
         vertices: { visible: false, fixed: true },
         ...polygon.styles,
      });

   });
};
function gArcDefault(params = {}) {
   return params.arcs.map((a) => {
      const arc = params.board.create('arc', gAllPointsDefault({ points: a.points, ...params }), a.style);
      if (a.name) {
         gTextDefault({ ...params, texts: [a.name] });
      };
      if (a.interactive) {
         arc.on('down', () => callback({ ...params, attractor: arc }));
      }
      return arc;
   });
};
function gTextDefault(params = {}) {
   return params.texts.map((t) => {
      const style = {
         fontSize: 20,
         fixed: true,
         ...t.style
      };
      const text = params.board.create('text', [t.x, t.y, t.text], style);
      if (t.interactive) {
         text.on('down', () => callback[callback["text"] ? "text" : "default"](def, board, id, text));
      };
      return text;
   });
};
//genera un board pasandole la referencia y las propiedades del board
function gBoard(def, boardSelect, id, style, callback = {}, i) {
   callback = {
      default: (def, board, id, ithem) => gFuntionDefault(def, board, id, ithem),
      ...callback,
   };
   const resultObj = {};
   style.reflectionAxie = {
      Y: style.reflectionAxie?.Y || false,
      X: style.reflectionAxie?.X || false,
      B: style.reflectionAxie?.B || false,
   };

   let board = JXG.JSXGraph.initBoard(id, {
      label: { visible: false },
      axis: style.axis[0] || false,
      boundingbox: style.boundingbox || [-4, 4, 4, -4],
      maxboundingbox: [-8, 8, 8, -8],
      grid: style.grid || false,
      grid: { strokeColor: !style.grid ? false : 'grey' },
      showNavigation: false,
      showCopyright: false,
      keyboard: {
         enabled: false,
         dy: 30,
         panShift: true,
         panCtrl: false,
      },
      pan: {
         needTwoFingers: true,
         enabled: false,
         needShift: true,
      },
      zoom: {
         needShift: false,
         pinchHorizontal: false,
         pinchVertical: false,
         pinchSensitivity: 0,
         min: 1000,
         max: 0,
         factorX: 0,
         factorY: 0,
         wheel: false,
      },
   });

   if (!style.axis[0] && style.axis[1]) {
      gAxies({
         def,
         board,
         axie: style.valueAxis?.xd,
         position: style.valueAxis?.xp,
         values: style.valueAxis?.xv,
         color: style.valueAxis?.colorx,
      });
   };

   if (!style.axis[0] && style.axis[2]) {
      gAxies({
         def,
         board,
         axie: style.valueAxis?.yd,
         position: style.valueAxis?.yp,
         values: style.valueAxis?.yv,
         color: style.valueAxis?.colory,
      });
   };

   const simetriAxies = [
      { points: [[-8, -8], [8, 8]], visible: style?.reflectionAxie?.B, interactive: false, typeCurve: "bisectriz", ...style.reflectionAxie.B }, //bisectriz
      { points: [[-8, 0], [8, 0]], visible: style?.reflectionAxie?.X, interactive: false, typeCurve: "eje_x", ...style.reflectionAxie.X }, //eje x
      { points: [[0, -8], [0, 8]], visible: style?.reflectionAxie?.Y, interactive: false, typeCurve: "eje_y", ...style.reflectionAxie.Y }, // eje Y
   ];

   reflectionAxie = gLineDefault({ def, board, id, lines: simetriAxies, callback: callback.line || callback.default });
   //agrego los ejes al def
   def.reflectionAxie = {
      axieB: reflectionAxie[0],
      axieX: reflectionAxie[1],
      axieY: reflectionAxie[2]
   };

   //pintar lista de points
   if (boardSelect.points) {
      resultObj.points = gAllPointsDefault({
         callback: callback.point || callback.default,
         def,
         board,
         id,
         points: boardSelect.points,
         i,
      });
   };
   //pintar lista de lineas
   if (boardSelect.lines) {
      resultObj.lines = gLineDefault({
         callback: callback.line || callback.default,
         def,
         board,
         id,
         lines: boardSelect.lines,
      });
   };
   //pintar lista de curvas
   if (boardSelect.curves) {
      resultObj.curves = gCurveDefault({
         callback: callback.curve || callback.default,
         def,
         board,
         id,
         curves: boardSelect.curves,
      });
   };

   //pintar lista de textos
   if (boardSelect.texts) {
      resultObj.texts = gTextDefault({
         callback: callback.text || callback.default,
         def,
         board,
         id,
         texts: boardSelect.texts,
      });
   };

   if (boardSelect.polygons) {
      resultObj.polygon = gPolygon({
         callback: callback.polygon || callback.default,
         def,
         board,
         id,
         polygons: boardSelect.polygons,
      });
   };

   if (boardSelect.arcs) {
      resultObj.arcs = gArcDefault({
         callback: callback.arc || callback.default,
         def,
         board,
         id,
         arcs: boardSelect.arcs,
      });
   };

   return { board, resultObj };
};
//agrega avisos sobre ek board
function gAlerts(def, id, text, type = 1, size = 15) {

   if (def.textAlert) {
      def.textAlert.remove();
      def.textAlert = null;
   };

   const textAlert = document.createElement('p');
   textAlert.textContent = text;

   textAlert.classList.add(
      type == 1 ? 'passInLibrary' : 'failedInLibrary',
      'justify-Content-center',
      'centerFloat',
      'pr-1',
      'pl-1',
   );
   textAlert.style.fontSize = size + 'px';
   document.querySelector('#' + id).appendChild(textAlert);
   def.textAlert = textAlert;
};
//compara dos valores position  con cierta olgura 
function gInterPoint(value, compare, noise = 0.1) {
   if (
      parseFloat(value.toFixed(2)) <= parseFloat(compare.toFixed(2)) + noise &&
      parseFloat(value.toFixed(2)) >= parseFloat(compare.toFixed(2)) - noise
   ) {
      return true;
   } else {
      return false;
   };
};

function gFuntionDefault(n, m, c) {
   console.log("************ Default *************");
   console.log(n);
   console.log(m);
   console.log(c);
};
//agrega un boton activo a la definicion ademas del toggle visual
function gButtonToggle(p = {}, params = { toggleResect: true, option: true, buttonActive: "buttonActive", ...p }) {
   if (params.def[params.buttonActive] != params.button) {
      if (params.def[params.buttonActive] != null) {
         params.def[params.buttonActive].classList.toggle('buttonDownActive');
      };
      params.def.mode = params.mode;

      params.def[params.buttonActive] = params.button;
      if (params.option) {
         params.def[params.buttonActive].classList.toggle('buttonDownActive');
      };
   } else {
      params.def[params.buttonActive].classList.toggle('buttonDownActive');
      if (params.toggleResect) {
         params.def.mode = null;
         params.def.iMod = null;
         params.def[params.buttonActive] = null;
      };
   };
};

//funcion revursiva recorre todo el objeto y sus hijos para retornar un elemento html completo
function gRecurcibeObjectHtml(iteration) {
   const element = document.createElement(iteration.type || "div");
   gSetAttributeDivs(element, iteration);
   if (iteration.chields != undefined) {
      for (divIter of Object.values(iteration.chields)) {
         element.appendChild(gRecurcibeObjectHtml(divIter));
      };
   };
   return element;
};
//setea los valores id, clases,data set del elemento
function gSetAttributeDivs(element, att) {
   if (att.id) {
      element.setAttribute("id", att.id);
   };

   if (att.attributes) {
      for (const p of Object.keys(att.attributes)) {
         element.setAttribute(p, att.attributes[p]);
      };

   };

   if (att.class) {
      element.className = att.class + `${(att.type == "button") ? " button-marg buttonKey" : ""} 
		${(att.dataSet && (-1 != att.dataSet.findIndex((e) => e[0] === "text"))) ? "buttonText" : ""}
		${(att.dataSet && (-1 != att.dataSet.findIndex((e) => e[0] === "tool"))) ? "buttonTool" : ""}`;
   };
   if (att.title) {
      element.setAttribute("title", att.title);
   };
   if (att.dataSet) {
      att.dataSet.forEach((data) => {
         element.setAttribute("data-" + data[0], data[1]);
      });
   };
   if (att.styles) {
      element.style = att.styles;
   };
   if (att.value) {
      element.value = att.value;
   };
   element.textContent = att.content;
};

function gMakeModal(params) {

   const modalTmp = `
   <div id="myModal" class="modal">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="top-bar" >
            <buttom class="closed close buttonAux"></buttom>
            <H3 id="title-modal">modal default</H3> 
         </div >
         <p id="text-modal">Alcaravan</p>
      </div >
   </div > `;

   document.body.insertAdjacentHTML('afterend', modalTmp);
   const modal = document.getElementById("myModal");
   modal.addEventListener("click", (e) => {
      if (e.target.classList.contains("close")) {
         (() => modal.style.display = "none")();
      };
   });
   window.onclick = function (e) {
      if (e.target == modal) {
         modal.style.display = "none";
      };
   };
   return modal;
};

//compara una curva con un array de puntos [[x,y],[x,y],[x,y]]
function gCompareCurves(curve, points, mode = false, noiseY = 0.2, noiseX = 0.2) {
   if (curve) {
      const allPoints = curve.points;
      const pointsCoord = allPoints.map((item) => { //obteniendo array con estructura mas manejable
         return [item.usrCoords[1], item.usrCoords[2]];
      });
      let condIni = null, condFinish = null;
      return points.every((coordDefault) => { // comparacion de puntos por defecto con puntos de la curva creada por el usuario
         const coordCurveUsr = pointsCoord.findIndex((item) => {//busca un elemento que coinsida
            return (
               (item[0] > (coordDefault[0] - noiseX) && item[0] < (coordDefault[0] + noiseX)) &&
               (item[1] > (coordDefault[1] - noiseY) && item[1] < (coordDefault[1] + noiseY)));
         });
         if (mode || coordCurveUsr > Math.round(pointsCoord.length * 0.75)) { //compara que al menos 1 de esos indices este por encima del 80% de la curva
            condFinish = true;
         };
         if (mode || coordCurveUsr < Math.round(pointsCoord.length * 0.25)) {//compara que al menos 1 de esos indices este por debajo del  20% de la curva
            condIni = true;
         };
         return coordCurveUsr != -1;
      }) && condIni === true && condFinish === true;
   };
};

function gAddPoint(def, board, color = '#D55E00') {
   if (def.curves === undefined || def.points === undefined) {
      def.points = [[]];
      def.curves = [];
   };
   if (def.maxCurves <= def.curves.length || def.mode != 4) {
      return;
   };
   if (siteMode === undefined) {
      var siteMode = "desktop";
   };
   const elementIn = board.getAllUnderMouse();
   if (elementIn.findIndex((p) => !Array.isArray(p) && p.elType === 'point') !== -1) {
      return;
   };
   const size = siteMode == 'desktop' ? 2.5 : 4;
   let coords = board.getUsrCoordsOfMouse(),
      x = coords[0],
      y = coords[1];
   const pointData = board.create('point',
      [x, y],
      {
         visible: (() => def.mode == 4),
         size: size,
         color: color,
         opacity: 0.8,
         name: '',
         fixed: false,
         isDraggable: false
      },
   );

   if (def.conditions?.road?.curve?.same) {
      const b = def.reflectionAxie.axieB;
      const tranf = board.create('transform', [b], { type: 'reflect' });
      def.samePoints.at(-1).push(board.create('point', [pointData, tranf], {
         name: '', color: 'green', visible: (() => def.debug && def.mode == 4)
      }));
      board.update();
   };
   def.points.at(-1).push(pointData);
};

function gAddCurv(def, board, points = false) {

   if (siteMode === undefined) {
      var siteMode = "desktop";
   };

   const size = siteMode == 'desktop' ? 2.5 : 3;
   if (def.maxCurves <= def.curves.length ||
      !points && (!def.points.at(-1) || def.points.at(-1).length < 2)
   ) {
      return;
   };

   def.points[def.points.length - 1].sort(
      (e, f) => e.coords.usrCoords[1] - f.coords.usrCoords[1],
   );

   let curvepoints = !points ? def.points.at(-1).map((e) => e) : points.map((e) => e);

   let curva = board.create(
      'cardinalspline',
      [curvepoints, 0.8, 'centripetal'],
      { strokeColor: def.curveMod.color || 'black', strokeWidth: size },
   );

   curva.interactive = true;
   board.update();
   /////////////////////////////////// direccionar al def /////////////////////////////////////////
   if (!points) {
      def.samePoints.push([]);
      def.curves.push(curva);
      def.points.push([]);
   };
   return curva;
};

function gExtractCoords(p) {
   return [parseFloat(p.X().toFixed(3)), parseFloat(p.Y().toFixed(3))];
};

function gHelpMsg(def, artifact, ref) {
   //funcion para desplegar un panel dentro del board
   const fragmen = new DocumentFragment("div");
   const btnAll = artifact.querySelector(`.btn-all`);
   const content = typeof def.helpMsg === 'object' ? def.helpMsg : 'Trabajo Realizado "Por el derecho a comprender"';
   const msg = `<div class="etwArtifact__help-msg" id="${ref}-helpMsg">
                  <div class="etwArtifact__help-msg-content">
                     <div class="etwArtifact__help-msg-btnClose-container">
                        <button class="etwArtifact__help-msg-btnClose btnCloseMsg button-marg buttonTertiary buttonKey" id="${ref}-closeMsg"></button>
                     </div>
                     <div class="etwArtifact__help-msg-textHelp" id="${ref}-containerHelpMsg"></div>
                     </div>
               </div>`;
   const div = document.createElement("div");
   div.style.position = "relative";
   div.classList.add("div-text-help");

   div.style = `  
    height: 100%!important;
    min-height: 100%; 
    position: absolute; 
    top: 0; 
    right: 0; 
    bottom: 0;
    width: 100%;
    display:none;
    `;

   fragmen.appendChild(div);
   div.insertAdjacentHTML("afterbegin", msg);
   const artifactMsg = fragmen.getElementById(`${ref}-helpMsg`);
   const textContainer = fragmen.getElementById(`${ref}-containerHelpMsg`);
   const btnCloseMsg = fragmen.getElementById(`${ref}-closeMsg`);


   if (content.addHtml === true) {
      textContainer.insertAdjacentHTML('afterend', content.html);
   } else {

      let contentTitle = document.createElement('h3');
      let contentText = document.createElement('p');

      contentText.style.marginTop = content.marginTop || '24px';
      contentText.style.fontSize = content.fontSize || '16px';
      contentTitle.appendChild(document.createTextNode(content.title ?? '<insertar titulo>'));
      contentText.appendChild(document.createTextNode(content.text ?? '<insertar texto en definicion>'));
      textContainer.appendChild(contentTitle);
      textContainer.appendChild(contentText);
   };

   artifact.insertBefore(fragmen, artifact.children[0]);
   btnAll.addEventListener('click', (e) => {
      if (e.target.classList.contains('help') || e.target.classList.contains('help-btn')) {

         div.style.display = "block";
         setTimeout(() => {
            artifactMsg.style.transform = 'translate(0%)';
         }, 100);

      };
   });

   btnCloseMsg.addEventListener('click', () => {
      artifactMsg.style.transform = 'translate(-100%)';
      setTimeout(() => {
         div.style.display = "none";
      }, 1000);
   });

};