const icDefBoards = {
   board_0: {
      style: {
         grid: true,
         origin: true,

         boundingbox: [-4, 4, 4, -4],
         axis: [false, true, true],
         valueAxis: {
            yd: [[0, 0], [0, 1]] /*dirección del eje y*/,
            xd: [[0, 0], [1, 0]],
            colory: "#000000",
            colorx: "#000000"
         },
      },
   },

};
const icDefBoards2 = {
   board_0: board.builder()
};

//si se va a agregar algo al objeto tiene que declararce la propiedad por defecto en el mod.js
const icDef = {
/*
   artifact_0: {
      helpMsg: {
         title: 'titulo',
         text: 'esto es un texto de prueba',
      },
      maxCurves: 2,
      conditions: {
         inputsToValidate: [

            {
               inputs: [
                  {
                     editable: false,
                     defaultXvalue: '-2',
                     
                     defaultYvalue: "2",

                  },

                  /*    {
                        defaultXvalue: '-1',                  
                        succesValue: '-1',
      
                     },
                     {
                        defaultXvalue: '0',
                      
                        defaultYvalue:null,
                        succesValue: '0',
      
                     }, 
                  {
                     defaultXvalue: '1',

                     defaultYvalue: null,
                     succesValue: '1',

                  },

               ],
               //infinities: [[8, 9], [2, 1]]

            },

            {
               inputs: [
                  {
                     defaultXvalue: '-2',
                     succesValue: '-3',
                     defaultYvalue: "2",

                  },

                  /*  {
                      defaultXvalue: '-1',                  
                      succesValue: '-3',
    
                   },
                   {
                      defaultXvalue: '0',
                    
                      defaultYvalue:null,
                      succesValue: '-3',
    
                   }, 
                  {
                     defaultXvalue: '1',

                     defaultYvalue: null,
                     succesValue: '-3',

                  },

               ],
              // infinities: [[8, 9, 10]]


            }
         ]

      },
   },
*/

   //artifact_0:artifact_0.factory(),
/*    
artifact_0: {
   helpMsg: {
      title: 'titulo',
      text: 'esto es un texto de prueba',
   },
   maxCurves: 2,
   conditions: {
      inputsToValidate: [

         {
            inputs: [
               {
                  defaultXvalue: '-2',
                  succesValue: '-2'
               },
                   {
                    defaultXvalue: '-1',                  
                     succesValue: '-1',
   
                  },
                  {
                     defaultXvalue: '0',
                   
                     defaultYvalue:null,
                     succesValue: '0',
   
                  }, 
               {
                  defaultXvalue: '1',

                  defaultYvalue: null,
                  succesValue: '1',

               },

            ],
           // infinities: [[8, 9], [2, 1]],
            // pointDefault:[[2,-1],[3,-2]]
         },

         {
            inputs: [
               {
                  defaultXvalue: '-2',
                  succesValue: '-3'
               },

                 {
                   defaultXvalue: '-1',                  
                   succesValue: '-3',
 
                },
                {
                   defaultXvalue: '0',
                 
                   defaultYvalue:null,
                   succesValue: '-3',
 
                }, 
               {
                  defaultXvalue: '1',

                  defaultYvalue: null,
                  succesValue: '-3',

               },

            ],
           // infinities: [[8, 9, 10]]


         }
      ]

   },
}, */
artifact_0:artifact_0.builder()
  /*
   artifact_1: {
      showTitle: true,
      artifactTitle: 'titulo',
      inputsDefault: [[[-0.35, 2], true, '', 1], [[2, -0.2], false, 'y', 2]],

      helpMsg: {
         title: 'titulo',
         text: 'esto es un texto de prueba',
      },
      textOfHelp: 'hi',
      conditions: {
         inputsToValidate: [
            {
               defaultXvalue: 'x',
               editable: true,
               //defaultYvalue:'hi', 
               editable: false,
               succesValue: '-x',
               //pointObject:null,
               //status:null,
            }
         ]
      }

   },
   artifact_3: {

      conditions: {

      }
   },*/
   /* 
   artifact_4: {

     conditions: {
        valueInputs:[
           [1,"f^{-1}\\left(x\\right)"],    
                    ],
     }
  },
  artifact_5: {

     conditions: {
        valueInputs:[
           [1,"g\\left(1.5\\right)"],    
                    ],
     }
  },
  artifact_6: {
     inputsDefault:[[[-0.35, 2], true, '', 1], [[2, -0.2], false, '2', 2]],

     conditions: {
        valueInputs:[
           [1,"x\\left(y\\right)"],    
                    ],
     }
  },
  
  artifact_7: {

     conditions: {
        valueInputs:[
           [1,"x\\left(f\\right)"],    
                    ],
     }
  },
  artifact_8: {
 
     conditions: {
        valueInputs:[
           [1,"g"],    
                    ],
     }
  }, */
};

icMain(icDef, icDefBoards2);


/*
 
 artifact_0: {
        helpMsg: {
           title:'titulo',
           text:'esto es un texto de prueba',
        },
        conditions: {  
           inputsToValidate:{
              inputs: [
                 {defaultXvalue: '-2',                  
                 succesValue: '-2'},

                 {
                    defaultXvalue: '-1',                  
                    succesValue: '-1',
  
                 },
                 {
                    defaultXvalue: '0',
                  
                    defaultYvalue:null,
                    succesValue: '0',
  
                 },
                 {
                    defaultXvalue: '1',
              
                    defaultYvalue:null,
                    succesValue: '1',
  
                 }, 

                
              
              ],
              infinities:[[10],null]
           }
        },
   },
   
 
*/